using UnityEngine;

namespace Project.Scripts
{
    public class BatMover : MonoBehaviour
    {
        [SerializeField] private float _maxX;

        private void Update()
        {
            float mouseX = Input.mousePosition.x;
            float xPos = Mathf.Clamp(mouseX / Screen.width * 12 - 6, -_maxX, _maxX);
            transform.position = new Vector3(xPos, transform.position.y, transform.position.z);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(Constants.BALL_TAG))
            {
                Vector3 hitPoint = other.ClosestPoint(transform.position);
                float xDiff = hitPoint.x - transform.position.x;
                Vector3 newDirection = new Vector3(xDiff, 0, 1).normalized;
                other.GetComponent<Rigidbody>().velocity = newDirection * other.GetComponent<Rigidbody>().velocity.magnitude;
            }
        }
    }
}
