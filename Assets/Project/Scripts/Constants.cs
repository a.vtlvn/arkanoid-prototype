public class Constants
{
    public const string BALL_TAG = "Ball";
    public const string BACKBOARD_TAG = "Backboard";
    public const string BAT_TAG = "Bat";
}
