using System;
using UnityEngine;

namespace Project.Scripts
{
    public class BallMover : MonoBehaviour
    {
        [SerializeField] private float _startSpeed;
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _acceleration;
        [SerializeField] private float _angle;
        [SerializeField] private float _startAngle;
        [SerializeField] private Rigidbody _rigidbody;
        private int _backboardCollidedCount;
        private int _bathCollidedCount;
        private Vector3 _velocity;
        public Action <int> OnCollideWithBat;
        public Action <int> OnCollideWithBackboard;
        

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Launch();
            }
        }

        private void FixedUpdate()
        {
            MovePlayer();
        }
        
        private void OnCollisionEnter(Collision collision) 
        {
            if (collision.transform.CompareTag(Constants.BAT_TAG))
            {
                Vector3 contactPoint = collision.contacts[0].point;
                float distanceFromCenter = contactPoint.x - transform.position.x;
                float angle = distanceFromCenter * _angle;
                Vector3 direction = Quaternion.Euler(0, angle, 0) * _velocity.normalized;
                _rigidbody.velocity = Vector3.Reflect(direction, collision.contacts[0].normal) * _velocity.magnitude;
                
                _bathCollidedCount++;
                OnCollideWithBat?.Invoke(_bathCollidedCount);
            }
            else if (collision.transform.CompareTag(Constants.BACKBOARD_TAG))
            {
                _backboardCollidedCount++;
                OnCollideWithBackboard?.Invoke(_backboardCollidedCount);
            }
            else
            {
                _rigidbody.velocity = Vector3.Reflect(_velocity, collision.contacts[0].normal);
            }
        }

        private void Launch()
        {
            Vector3 direction = Quaternion.Euler(0, _startAngle, 0) * Vector3.forward;
            _rigidbody.velocity = direction * _startSpeed;
        }

        private void MovePlayer()
        {
            if (_rigidbody.velocity.magnitude < _maxSpeed)
            {
                _rigidbody.velocity +=  _rigidbody.velocity.normalized * (_acceleration * Time.fixedDeltaTime);
            }
            else
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * _maxSpeed;
            }
            
            _velocity = _rigidbody.velocity;
        }
    }
}
