using System;
using TMPro;
using UnityEngine;

namespace Project.Scripts
{
    public class Hud : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _backboardText;
        [SerializeField] private TextMeshProUGUI _bathText;
        [SerializeField] private BallMover _ballMover;


        private void Start()
        {
            OnCollideWithBat(0);
            OnCollideWithBackboard(0);
        }

        private void OnEnable()
        { 
            _ballMover.OnCollideWithBackboard += OnCollideWithBackboard;
            _ballMover.OnCollideWithBat += OnCollideWithBat;
        }
        
        private void OnDisable()
        { 
            _ballMover.OnCollideWithBackboard -= OnCollideWithBackboard;
            _ballMover.OnCollideWithBat -= OnCollideWithBat;
        }

        private void OnCollideWithBat(int count)
        {
            _bathText.text = $"Bath collided: {count}";
        }

        private void OnCollideWithBackboard(int count)
        {
            _backboardText.text = $"Backboard collided: {count}";
        }
    }
}
